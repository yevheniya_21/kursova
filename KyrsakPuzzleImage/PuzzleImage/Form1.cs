﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PuzzleImage
{
    
    public partial class PlayGame : Form
    {
          public int mill, cek, min;
 

        public PlayGame(Image img)
        {
            InitializeComponent();
            image = img;
            groupBoxPuzzle.BackgroundImage = img;
        }
            OpenFileDialog openFileDialog = null;
            Image image;
            PictureBox picBox = null;

            public void buttonImageBrowse_Click(object sender, EventArgs e)
            {              

                if (openFileDialog == null)
                    openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                 
                    image = CreateBitmapImage(Image.FromFile(openFileDialog.FileName));

                    if (picBox == null)
                    {
                        picBox = new PictureBox();
                        picBox.Height = groupBoxPuzzle.Height;
                        picBox.Width = groupBoxPuzzle.Width;
                        groupBoxPuzzle.Controls.Add(picBox);
                    }
                    picBox.Image = image;

                }
            } 

       

        private Bitmap CreateBitmapImage(Image image)
        {         

            Bitmap objBmpImage = new Bitmap(groupBoxPuzzle.Width, groupBoxPuzzle.Height);
            Graphics objGraphics = Graphics.FromImage(objBmpImage);
            objGraphics.Clear(Color.White);
            objGraphics.DrawImage(image, new Rectangle(0, 0, groupBoxPuzzle.Width, groupBoxPuzzle.Height));
            objGraphics.Flush();

            return objBmpImage;
        }

        PictureBox[] picBoxes = null;
        Image[] images = null;
        const int LEVEL_1_NUM = 4;
        int currentLevel = 0;


        private void buttonlevel1_Click(object sender, EventArgs e)
        {
            currentLevel = LEVEL_1_NUM;
            labelstatus.Text = "Level 1 is ongoing...";
            PlayLevel();
        }

        private void PlayLevel()
        {

            if (picBox != null)
            {
                groupBoxPuzzle.Controls.Remove(picBox);
                picBox.Dispose();
                picBox = null;
            }

            if (picBoxes == null)
            {
                images = new Image[currentLevel];
                picBoxes = new PictureBox[currentLevel];
            }

            int numRow = (int)Math.Sqrt(currentLevel);
            int numCol = numRow;
            int unitX = groupBoxPuzzle.Width / numRow;
            int unitY = groupBoxPuzzle.Height / numCol;
            int[] indice = new int[currentLevel];

            for (int i = 0; i < currentLevel; i++)
            {
                indice[i] = i;
                if (picBoxes[i] == null)
                {
                    picBoxes[i] = new MyPicturesBox();
                    picBoxes[i].Click += new EventHandler(OnPuzzleClick);
                    picBoxes[i].BorderStyle = BorderStyle.Fixed3D;
                }

                picBoxes[i].Width = unitX;
                picBoxes[i].Height = unitY;

                ((MyPicturesBox)picBoxes[i]).Index = i;

                CreateBitmapImage(image, images, i, numRow, numCol, unitX, unitY);

                picBoxes[i].Location = new Point(unitX * (i % numCol), unitY * (i / numCol));
                if (!groupBoxPuzzle.Controls.Contains(picBoxes[i]))
                    groupBoxPuzzle.Controls.Add(picBoxes[i]);
            }

            NewPuzzle(ref indice);
            for (int i = 0; i < currentLevel; i++)
            {
                picBoxes[i].Image = images[indice[i]];
                ((MyPicturesBox)picBoxes[i]).ImageIndex = indice[i];
            }
        }

        MyPicturesBox firstBox = null;
        MyPicturesBox secondBox = null;

        public void OnPuzzleClick(object sender, EventArgs e)
        {
            if (firstBox == null)
            {
                firstBox = (MyPicturesBox)sender;
                firstBox.BorderStyle = BorderStyle.FixedSingle;
            }
            else if (secondBox == null)
            {
                secondBox = (MyPicturesBox)sender;
                firstBox.BorderStyle = BorderStyle.Fixed3D;
                secondBox.BorderStyle = BorderStyle.FixedSingle;
                SwitchImage(firstBox, secondBox);
                firstBox = null;
                secondBox = null;
            }
                  }

        private bool isSuccessful()
        {
            for (int i = 0; i < currentLevel; i++)
            {
                if (((MyPicturesBox)picBoxes[i]).ImageIndex != ((MyPicturesBox)picBoxes[i]).Index)
                    return false;
            }
            return true;
        }

        private void SwitchImage(MyPicturesBox box1, MyPicturesBox box2)
        {
            int tmp = box2.ImageIndex;
            box2.Image = images[box1.ImageIndex];
            box2.ImageIndex = box1.ImageIndex;
            box1.Image = images[tmp];
            box1.ImageIndex = tmp;
            if (isSuccessful())
            {
                labelstatus.Text = "Well Done!!!";
                const string message = "You WON ";
                const string caption = "Your Won!!!Try game? ";
                MessageBoxButtons resalt = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(caption, message, resalt);
                
                if (result == System.Windows.Forms.DialogResult.No)
                {
                    this.Close();
                    Close();

                }
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    ChooseImage f3 = new ChooseImage();
                    f3.Show();
                    this.Close();
                }
            }
        }


        private void CreateBitmapImage(Image image, Image[] images, int index, int numRow, int numCol, int unitX, int unitY)
        {
            images[index] = new Bitmap(unitX, unitY);
            Graphics objGraphics = Graphics.FromImage(images[index]);
            objGraphics.Clear(Color.White);

            objGraphics.DrawImage(image,
                new Rectangle(0, 0, unitX, unitY),
                new Rectangle(unitX * (index % numCol), unitY * (index / numCol), unitX, unitY),
                GraphicsUnit.Pixel);
            objGraphics.Flush();
        }

        private void NewPuzzle(ref int[] array)
        {
            Random rng = new Random();
            int n = array.Length;
            while (n > 1)
            {
                int k = rng.Next(n);
                n--;
                int temp = array[n];
                array[n] = array[k];
                array[k] = temp;

            }
        }

        private void buttonlevel2_Click(object sender, EventArgs e)
        {

            const int LEVEL_2_NUM = 9;
            currentLevel = LEVEL_2_NUM;
            labelstatus.Text = "Level 2 is ongoing...";

            if (picBox != null)
            {
                groupBoxPuzzle.Controls.Remove(picBox);
                picBox.Dispose();
                picBox = null;
            }

            if (picBoxes == null)
            {
                images = new Image[currentLevel];
                picBoxes = new PictureBox[currentLevel];
            }

            int numRow = (int)Math.Sqrt(currentLevel);
            int numCol = numRow;
            int unitX = groupBoxPuzzle.Width / numRow;
            int unitY = groupBoxPuzzle.Height / numCol;
            int[] indice = new int[currentLevel];

            for (int i = 0; i < currentLevel; i++)
            {
                indice[i] = i;
                if (picBoxes[i] == null)
                {
                    picBoxes[i] = new MyPicturesBox();
                    picBoxes[i].Click += new EventHandler(OnPuzzleClick);
                    picBoxes[i].BorderStyle = BorderStyle.Fixed3D;
                }

                picBoxes[i].Width = unitX;
                picBoxes[i].Height = unitY;

                ((MyPicturesBox)picBoxes[i]).Index = i;

                CreateBitmapImage(image, images, i, numRow, numCol, unitX, unitY);

                picBoxes[i].Location = new Point(unitX * (i % numCol), unitY * (i / numCol));
                if (!groupBoxPuzzle.Controls.Contains(picBoxes[i]))
                    groupBoxPuzzle.Controls.Add(picBoxes[i]);
            }

            NewPuzzle(ref indice);
            for (int i = 0; i < currentLevel; i++)
            {
                picBoxes[i].Image = images[indice[i]];
                ((MyPicturesBox)picBoxes[i]).ImageIndex = indice[i];
            }
        }

        private void buttonlevel3_Click(object sender, EventArgs e)
        {
            {

                const int LEVEL_3_NUM = 16;
                currentLevel = LEVEL_3_NUM;
                labelstatus.Text = "Level 3 is ongoing...";

                if (picBox != null)
                {
                    groupBoxPuzzle.Controls.Remove(picBox);
                    picBox.Dispose();
                    picBox = null;
                }

                if (picBoxes == null)
                {
                    images = new Image[currentLevel];
                    picBoxes = new PictureBox[currentLevel];
                }

                int numRow = (int)Math.Sqrt(currentLevel);
                int numCol = numRow;
                int unitX = groupBoxPuzzle.Width / numRow;
                int unitY = groupBoxPuzzle.Height / numCol;
                int[] indice = new int[currentLevel];

                for (int i = 0; i < currentLevel; i++)
                {
                    indice[i] = i;
                    if (picBoxes[i] == null)
                    {
                        picBoxes[i] = new MyPicturesBox();
                        picBoxes[i].Click += new EventHandler(OnPuzzleClick);
                        picBoxes[i].BorderStyle = BorderStyle.Fixed3D;
                    }

                    picBoxes[i].Width = unitX;
                    picBoxes[i].Height = unitY;

                    ((MyPicturesBox)picBoxes[i]).Index = i;

                    CreateBitmapImage(image, images, i, numRow, numCol, unitX, unitY);

                    picBoxes[i].Location = new Point(unitX * (i % numCol), unitY * (i / numCol));
                    if (!groupBoxPuzzle.Controls.Contains(picBoxes[i]))
                        groupBoxPuzzle.Controls.Add(picBoxes[i]);
                }

                NewPuzzle(ref indice);
                for (int i = 0; i < currentLevel; i++)
                {
                    picBoxes[i].Image = images[indice[i]];
                    ((MyPicturesBox)picBoxes[i]).ImageIndex = indice[i];
                }
            }
        }

        private void buttonlevel4_Click(object sender, EventArgs e)
        {

            {

                const int LEVEL_3_NUM = 36;
                currentLevel = LEVEL_3_NUM;
                labelstatus.Text = "Level 4 is ongoing...";

                if (picBox != null)
                {
                    groupBoxPuzzle.Controls.Remove(picBox);
                    picBox.Dispose();
                    picBox = null;
                }

                if (picBoxes == null)
                {
                    images = new Image[currentLevel];
                    picBoxes = new PictureBox[currentLevel];
                }

                int numRow = (int)Math.Sqrt(currentLevel);
                int numCol = numRow;
                int unitX = groupBoxPuzzle.Width / numRow;
                int unitY = groupBoxPuzzle.Height / numCol;
                int[] indice = new int[currentLevel];

                for (int i = 0; i < currentLevel; i++)
                {
                    indice[i] = i;
                    if (picBoxes[i] == null)
                    {
                        picBoxes[i] = new MyPicturesBox();
                        picBoxes[i].Click += new EventHandler(OnPuzzleClick);
                        picBoxes[i].BorderStyle = BorderStyle.Fixed3D;
                    }

                    picBoxes[i].Width = unitX;
                    picBoxes[i].Height = unitY;

                    ((MyPicturesBox)picBoxes[i]).Index = i;

                    CreateBitmapImage(image, images, i, numRow, numCol, unitX, unitY);

                    picBoxes[i].Location = new Point(unitX * (i % numCol), unitY * (i / numCol));
                    if (!groupBoxPuzzle.Controls.Contains(picBoxes[i]))
                        groupBoxPuzzle.Controls.Add(picBoxes[i]);
                }

                NewPuzzle(ref indice);
                for (int i = 0; i < currentLevel; i++)
                {
                    picBoxes[i].Image = images[indice[i]];
                    ((MyPicturesBox)picBoxes[i]).ImageIndex = indice[i];
                }
            }
        }


        private void Timer1_Tick(object sender, EventArgs e)
        {
            cek += 1;
            if (cek == 60) {
                cek = 0;
                min += 1;
            }
            if (cek == 5) {


                timer1.Enabled = false;
                label1.Text = 0 + " : " + 0;

                const string message =
         "You lost ";
                const string caption = "Your time is up!!! Try game?" ;
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(caption, message, buttons);

                if (result == System.Windows.Forms.DialogResult.No)
                {
                    this.Close();
                    Close();

                }
                if (result == System.Windows.Forms.DialogResult.Yes)
                {

                    ChooseImage f3 = new ChooseImage();
                    f3.Show();
                     this.Close();
                    }
            }
            label1.Text = min + " : " + cek ;
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

    }

}
