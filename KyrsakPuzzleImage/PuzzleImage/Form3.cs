﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace PuzzleImage
{
    public partial class ChooseImage : Form
    {
        
        public ChooseImage()
        {
           InitializeComponent();
        }

        private void PictureBoxClick(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            PlayGame F1 = new PlayGame(pb.Image);
            F1.Show();
            this.Close();
        }
    }
}
