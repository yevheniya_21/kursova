﻿namespace PuzzleImage
{
    partial class PlayGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayGame));
            this.groupBoxPuzzle = new System.Windows.Forms.GroupBox();
            this.groupBoxPlay = new System.Windows.Forms.GroupBox();
            this.buttonlevel4 = new System.Windows.Forms.Button();
            this.buttonlevel3 = new System.Windows.Forms.Button();
            this.buttonlevel2 = new System.Windows.Forms.Button();
            this.buttonlevel1 = new System.Windows.Forms.Button();
            this.groupBoxStatus = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timerLabel = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.labelstatus = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBoxPlay.SuspendLayout();
            this.groupBoxStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxPuzzle
            // 
            this.groupBoxPuzzle.BackgroundImage = global::PuzzleImage.Properties.Resources.icopuzzle;
            this.groupBoxPuzzle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.groupBoxPuzzle.Location = new System.Drawing.Point(12, 32);
            this.groupBoxPuzzle.Name = "groupBoxPuzzle";
            this.groupBoxPuzzle.Size = new System.Drawing.Size(582, 353);
            this.groupBoxPuzzle.TabIndex = 1;
            this.groupBoxPuzzle.TabStop = false;
            this.groupBoxPuzzle.Text = "Puzzle";
            // 
            // groupBoxPlay
            // 
            this.groupBoxPlay.Controls.Add(this.buttonlevel4);
            this.groupBoxPlay.Controls.Add(this.buttonlevel3);
            this.groupBoxPlay.Controls.Add(this.buttonlevel2);
            this.groupBoxPlay.Controls.Add(this.buttonlevel1);
            this.groupBoxPlay.Location = new System.Drawing.Point(629, 42);
            this.groupBoxPlay.Name = "groupBoxPlay";
            this.groupBoxPlay.Size = new System.Drawing.Size(200, 209);
            this.groupBoxPlay.TabIndex = 2;
            this.groupBoxPlay.TabStop = false;
            this.groupBoxPlay.Text = "Play";
            // 
            // buttonlevel4
            // 
            this.buttonlevel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonlevel4.ForeColor = System.Drawing.Color.MediumBlue;
            this.buttonlevel4.Image = ((System.Drawing.Image)(resources.GetObject("buttonlevel4.Image")));
            this.buttonlevel4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonlevel4.Location = new System.Drawing.Point(10, 156);
            this.buttonlevel4.Name = "buttonlevel4";
            this.buttonlevel4.Size = new System.Drawing.Size(176, 42);
            this.buttonlevel4.TabIndex = 3;
            this.buttonlevel4.Text = "Level4";
            this.buttonlevel4.UseVisualStyleBackColor = true;
            this.buttonlevel4.Click += new System.EventHandler(this.buttonlevel4_Click);
            // 
            // buttonlevel3
            // 
            this.buttonlevel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonlevel3.ForeColor = System.Drawing.Color.MediumBlue;
            this.buttonlevel3.Image = ((System.Drawing.Image)(resources.GetObject("buttonlevel3.Image")));
            this.buttonlevel3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonlevel3.Location = new System.Drawing.Point(10, 111);
            this.buttonlevel3.Name = "buttonlevel3";
            this.buttonlevel3.Size = new System.Drawing.Size(176, 42);
            this.buttonlevel3.TabIndex = 2;
            this.buttonlevel3.Text = "Level3";
            this.buttonlevel3.UseVisualStyleBackColor = true;
            this.buttonlevel3.Click += new System.EventHandler(this.buttonlevel3_Click);
            // 
            // buttonlevel2
            // 
            this.buttonlevel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonlevel2.ForeColor = System.Drawing.Color.MediumBlue;
            this.buttonlevel2.Image = ((System.Drawing.Image)(resources.GetObject("buttonlevel2.Image")));
            this.buttonlevel2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonlevel2.Location = new System.Drawing.Point(10, 65);
            this.buttonlevel2.Name = "buttonlevel2";
            this.buttonlevel2.Size = new System.Drawing.Size(176, 42);
            this.buttonlevel2.TabIndex = 1;
            this.buttonlevel2.Text = "Level2";
            this.buttonlevel2.UseVisualStyleBackColor = true;
            this.buttonlevel2.Click += new System.EventHandler(this.buttonlevel2_Click);
            // 
            // buttonlevel1
            // 
            this.buttonlevel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonlevel1.ForeColor = System.Drawing.Color.MediumBlue;
            this.buttonlevel1.Image = ((System.Drawing.Image)(resources.GetObject("buttonlevel1.Image")));
            this.buttonlevel1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonlevel1.Location = new System.Drawing.Point(10, 19);
            this.buttonlevel1.Name = "buttonlevel1";
            this.buttonlevel1.Size = new System.Drawing.Size(176, 42);
            this.buttonlevel1.TabIndex = 0;
            this.buttonlevel1.Text = "Level1";
            this.buttonlevel1.UseVisualStyleBackColor = true;
            this.buttonlevel1.Click += new System.EventHandler(this.buttonlevel1_Click);
            // 
            // groupBoxStatus
            // 
            this.groupBoxStatus.Controls.Add(this.label1);
            this.groupBoxStatus.Controls.Add(this.timerLabel);
            this.groupBoxStatus.Controls.Add(this.startButton);
            this.groupBoxStatus.Controls.Add(this.labelstatus);
            this.groupBoxStatus.Location = new System.Drawing.Point(629, 257);
            this.groupBoxStatus.Name = "groupBoxStatus";
            this.groupBoxStatus.Size = new System.Drawing.Size(200, 138);
            this.groupBoxStatus.TabIndex = 3;
            this.groupBoxStatus.TabStop = false;
            this.groupBoxStatus.Text = "Status";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(66, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "TIME";
            // 
            // timerLabel
            // 
            this.timerLabel.AutoSize = true;
            this.timerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.timerLabel.Location = new System.Drawing.Point(34, 103);
            this.timerLabel.Name = "timerLabel";
            this.timerLabel.Size = new System.Drawing.Size(0, 25);
            this.timerLabel.TabIndex = 2;
            // 
            // startButton
            // 
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButton.ForeColor = System.Drawing.Color.MediumBlue;
            this.startButton.Image = ((System.Drawing.Image)(resources.GetObject("startButton.Image")));
            this.startButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.startButton.Location = new System.Drawing.Point(10, 43);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(176, 43);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Game time";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // labelstatus
            // 
            this.labelstatus.AutoSize = true;
            this.labelstatus.Location = new System.Drawing.Point(71, 16);
            this.labelstatus.Name = "labelstatus";
            this.labelstatus.Size = new System.Drawing.Size(24, 13);
            this.labelstatus.TabIndex = 0;
            this.labelstatus.Text = "Idle";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(865, 428);
            this.Controls.Add(this.groupBoxStatus);
            this.Controls.Add(this.groupBoxPlay);
            this.Controls.Add(this.groupBoxPuzzle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GamePuzzle";
            this.groupBoxPlay.ResumeLayout(false);
            this.groupBoxStatus.ResumeLayout(false);
            this.groupBoxStatus.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxPuzzle;
        private System.Windows.Forms.GroupBox groupBoxPlay;
        private System.Windows.Forms.Button buttonlevel4;
        private System.Windows.Forms.Button buttonlevel3;
        private System.Windows.Forms.Button buttonlevel2;
        private System.Windows.Forms.Button buttonlevel1;
        private System.Windows.Forms.GroupBox groupBoxStatus;
        private System.Windows.Forms.Label timerLabel;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelstatus;
    }
}

